const login = (body) => {
  return axios
    .post(
      "http://61.19.201.20:19539/activity/quiz/login",
      JSON.stringify(body),
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    )
    .then((response) => {
      return response.data;
    })
    .catch((err) => {
      console.error(err);
    });
};

const refreshtoken = (token) => {
  let jsondata = {
    token: token,
  };
  return axios
    .post(
      "http://61.19.201.20:19539/activity/quiz/refreshtoken",
      JSON.stringify(jsondata),
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    )
    .then((response) => {
      return response.data;
    })
    .catch((err) => {
      console.error(err);
    });
};

const getQuizAnswer = (token) => {
  let jsondata = {
    token: token,
  };
  return axios
    .post(
      "http://61.19.201.20:19539/activity/quiz/answer",
      JSON.stringify(jsondata),
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    )
    .then((response) => {
      return response.data;
    })
    .catch((err) => {
      console.error(err);
    });
};

const getQuestions = () => {
  return axios
    .get("http://61.19.201.20:19539/activity/quiz/questions")
    .then((response) => {
      return response.data;
    })
    .catch((err) => {
      console.error(err);
    });
};

const getChoices = (questionid) => {
  let jsondata = {
    questionid: questionid,
  };
  return axios
    .post(
      "http://61.19.201.20:19539/activity/quiz/choices",
      JSON.stringify(jsondata),
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    )
    .then((response) => {
      return response.data;
    })
    .catch((err) => {
      console.error(err);
    });
};

const sendAnswer = (token, answerbody) => {
  let jsondata = {
    token: token,
    answerbody: answerbody,
  };
  return axios
    .post(
      "http://61.19.201.20:19539/activity/quiz/sendanswer",
      JSON.stringify(jsondata),
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    )
    .then((response) => {
      return response.data;
    })
    .catch((err) => {
      console.error(err);
    });
};
