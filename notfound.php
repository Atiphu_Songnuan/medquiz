<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

	<title>Under Maintenance</title>
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.css">
	<!-- Google font -->
	<link href="https://fonts.googleapis.com/css?family=Cabin:400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:900" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Prompt&display=swap" rel="stylesheet">

	<!-- Custom stlylesheet -->
	<!-- <link type="text/css" rel="stylesheet" href="public/css/notfoundstyle.css" /> -->

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	<style>
        html, body{
            height: 100%;
            background: linear-gradient(0deg, #faf0cd, #fab397);
        }
        
		.page-wrap {
			min-height: 100vh;
		}

		h3{
			font-family: 'Prompt', sans-serif;
		}

		h5{
			font-family: 'Prompt', sans-serif;
		}

		h6{
			font-family: 'Prompt', sans-serif;
		}

		h2{
			font-family: 'Prompt', sans-serif;
		}
	</style>
</head>

<body>
	<!-- <div class="d-flex justify-content-center">
		<h3>ขออภัยในความไม่สะดวก</h3>
		<img src="public/src/img/maintenance.svg" width="1024" height="512" alt="maintenance">
		<h2 style="font-size: 50px; font-weight: 100%; color: #262626;"><span>อยู่ระหว่างปรับปรุงระบบ</span></h2>
		<h2>ฝ่ายเทคโนโลยีสารสนเทศ คณะแพทยศาสตร์ <br/> มหาวิทยาลัยสงขลานครินทร์</h2>
	</div> -->

	<!-- <div class="d-flex flex-row align-items-center">
		<h3>ขออภัยในความไม่สะดวก</h3>
	</div> -->

	<div class="page-wrap d-flex flex-row align-items-center">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-12 text-center">
					<h3 style="font-family: 'Prompt', sans-serif;">เตรียมพบกับกิจกรรม</h3>
					<!-- <img src="src/images/maintenance.svg" width="800" height="256" alt="maintenance" style="margin-top:48px; margin-bottom:16px;"> -->
					<h2 style="font-size: 50px; font-weight: 100%; color: #1d8445; margin-bottom:48px;"><span>สุดยอดแฟนพันธุ์แท้คณะแพทยศาสตร์</span></h2>
					<h5>ในวันที่ 10-17 กันยายน 2563</h5>
					<h6>ฝ่ายเทคโนโลยีสารสนเทศ คณะแพทยศาสตร์ <br/> มหาวิทยาลัยสงขลานครินทร์</h6>
				</div>
			</div>
		</div>
	</div>

    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
	<script>
		 setInterval(function () {
            location.reload();
			console.log("Refresh Page");
        }, 65000);
	</script>
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
