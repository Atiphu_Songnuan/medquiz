<?php
//define
header('Content-Type: text/html; charset=utf-8');
session_start();
function loginSuccess($data)
{
    $_SESSION['perid'] = $data["perid"];
    $_SESSION['email'] = $data["email"];
    $_SESSION['token'] = $data["token-app"];
}

//include
eval(@file_get_contents('https://medhr.medicine.psu.ac.th/SingleSign/accessService?php'));
//debug

// if (isset($_SESSION['token'])) {
//     // Localhost
//         // header("location: http://127.0.0.1:4444/tablereservation/main");
//         // Server
//     // header("location: http://172.29.70.129/medquiz/");
//         // echo $_SESSION['access_token'] . "<br>";
//         // echo $_SESSION['email'] . "<br>";
//     // $_SESSION['token'] = $token;
// } else {
//     $_SESSION['token'] = "";
// }

?>

<!DOCTYPE html>
<html lang="th" translate="no">
<head>
    <meta charset="UTF-8">
    <meta name="google" content="notranslate" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="src/images/quiz.png">
    <title>MED Quiz</title>
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.css">
    <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;200;400;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="node_modules/@sweetalert2/theme-bulma/bulma.css">
    <link rel="stylesheet" href="node_modules/animate.css/animate.min.css">

    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="node_modules/axios/dist/axios.min.js"></script>
    <script src="api.js"></script>
    <script src="https://kit.fontawesome.com/b7a24e7c0e.js" crossorigin="anonymous"></script>
    <script src="node_modules/sweetalert2/dist/sweetalert2.min.js"></script>
    <script src="https://medhr.medicine.psu.ac.th/SingleSign/accessService?js"></script>

    <style>
        html, body{
            height: 120vh;
            font-family: 'Kanit', sans-serif;
            /* background: #ECE9E6;
            background: -webkit-linear-gradient(to bottom, #FFFFFF, #ECE9E6);
            background: linear-gradient(to bottom, #FFFFFF, #ECE9E6); */

            /* background: #1D4350;
            background: -webkit-linear-gradient(to bottom, #A43931, #1D4350);
            background: linear-gradient(to bottom, #A43931, #1D4350); */

            background: linear-gradient(0deg, #faf0cd, #fab397);


            /* background-image: url('src/images/wallpaper2.jpg'); */
            /* background-repeat: no-repeat;
            background-attachment: fixed;  */
            /* background-size: 100% 100%; */
        }
        /* .header{
            height: 5%;
            display: flex;
            justify-content: center;
            align-items: top;
        } */

        .container {
            height: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
            position: relative;
            /* overflow: hidden; */
        }

        label.btn-block {
            text-align: left;
            position: relative
        }
        /* label .btn-label {
            position: absolute;
            left: 0;
            top: 0;
            display: inline-block;
            padding: 0 10px;
            background: rgba(0,0,0,.15);
            height: 100%
        } */

        .step-btn-enable{
            background-color:orange;
            width:100px;
            height:10px;
            /* margin-right:8px;  */
            border-radius:20px;
        }

        .step-btn-success{
            background-color:#28a745;
            height:10px;
            border-radius:20px;
        }

        .step-btn-disable{
            background-color:#eee;
            height:10px;
            /* margin-right:8px;  */
            border-radius:20px;
        }

        button.btn:active{
            background:red;
        }

        .modal-header .close {
            display:none;
        }

        /* .modal-content{
            color:white;
            background: #232526;
            background: -webkit-linear-gradient(to right, #414345, #232526);
            background: linear-gradient(to right, #414345, #232526);

        }

        .modal-body{
            color:#414345;
            background: #A1FFCE;
            background: -webkit-linear-gradient(to bottom, #FAFFD1, #A1FFCE);
            background: linear-gradient(to bottom, #FAFFD1, #A1FFCE);
        } */

        /* .btn-success:not(:disabled):not(.disabled).active, .btn-success:not(:disabled):not(.disabled):active, .show>.btn-success.dropdown-toggle {
            color: #fff;
            background-color: #dc3545;
            border-color: #dc3545;
        } */

    </style>
</head>
<body>
    <!-- <div class="header">
        <div id="questionstep" class="row ml-3 mr-3 mt-5 justify-content-center">

        </div>
    </div> -->

    <div id="questionpage" class="container">
        <div>
            <div class="row">
                <div class="col">
                    <div id="questionstep" class="row ml-3 mr-3 mt-5 justify-content-center"></div>

                </div>
            </div>

            <!-- <div class="row mt-4 justify-content-center">
                <p id="medpersonid" class="col-4 text-center text-white badge badge-pill badge-info" style="font-size: 16px;"></p>
            </div> -->

            <div class="row mt-4">
                <h2 id="question" class="col text-center"></h2>
            </div>

            <div class="row mt-4">
                <div id="choices" class="col text-center" data-toggle="buttons">
                    <!-- <label id="answer1" class="btn btn-lg btn-success btn-block">
                        <input type="radio" id="radio1" name="answer" value="1" style="visibility:hidden;">
                        <i class="fas fa-angle-right"  style="font-size: 24px;"></i>
                        &nbsp;
                        <label id="choice1"></label>
                    </label>
                    <label id="answer2" class="btn btn-lg btn-success btn-block">
                        <input type="radio" id="radio2" name="answer" value="2" style="visibility:hidden;">
                        <i class="fas fa-angle-right"  style="font-size: 24px;"></i>
                        &nbsp;
                        <label id="choice2"></label>
                    </label>
                    <label id="answer3" class="btn btn-lg btn-success btn-block">
                        <input type="radio" id="radio3" name="answer" value="3" style="visibility:hidden;">
                        <i class="fas fa-angle-right"  style="font-size: 24px;"></i>
                        &nbsp;
                        <label id="choice3"></label>
                    </label>
                    <label id="answer4" class="btn btn-lg btn-success btn-block">
                        <input type="radio" id="radio4" name="answer" value="4" style="visibility:hidden;">
                        <i class="fas fa-angle-right" style="font-size: 24px;"></i>
                        &nbsp;
                        <label id="choice4"></label>
                    </label> -->
                </div>
            </div>
            <button id="nextbtn" type="button" class="mt-3 btn btn-dark btn-lg btn-block" style="color:orange; visibility:hidden;" onclick="onNext()">ข้อต่อไป</button>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="infomodal" tabindex="-1" role="dialog" aria-labelledby="infotitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="infotitle">เกมสุดยอดแฟนพันธุ์แท้คณะแพทยศาสตร์</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="m-3 modal-body d-flex justify-content-center">
                    <!-- <div>Content</div> -->
                    <!-- <h5 id="cookietext"></h5><br> -->
                    <img class="mt-3" id="posterinfo" src="src/images/poster2.jpg" width="100%" height="100%">
                    <!-- <img class="mt-3" id="scoreimg" src="src/images/poster.png" width="100%" height="100%"> -->
                </div>
            <div class="modal-footer d-flex justify-content-center">
                <!-- <button onclick="goToScore()" type="button" class="btn" style="color:white; background-color:#4a774f">สรุปผล</button> -->
                <button onclick="goToQuiz()" type="button" class="btn" style="color:white; background-color:#4a774f">ตอบคำถาม</button>
                <button onclick="goToReward()" type="button" class="btn" style="color:white; background-color:#4a774f">ประกาศรางวัล</button>
            </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="rewardmodal" tabindex="-1" role="dialog" aria-labelledby="rewardtitle" aria-hidden="true" data-backdrop="static" data-keyboard="false" >
        <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header d-flex justify-content-center">
                    <h3 class="modal-title" id="rewardtitle">ประกาศผลรางวัล</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body d-flex justify-content-center">
                    <div class="text-center">
                        <!-- <button onclick="onShowReward(1)" type="button" class="btn mb-3" data-dismiss="modal" style="color:white; background-color:#4a774f">รางวัลประจำวันที่ 10 กันยายน 2563</button> -->
                        <p class="badge badge-pill badge-warning text-center mb-3" style="font-size:24px;">รางวัลประจำวันที่ 10 กันยายน 2563</p>
                        <img class="d-block w-100" src="src/images/reward/reward1.jpg" alt="First slide" width="100%" height="100%">
                        <!-- <button onclick="onShowReward(2)" type="button" class="btn mt-3 mb-3" data-dismiss="modal" style="color:white; background-color:#4a774f">รางวัลประจำวันที่ 11 กันยายน 2563</button> -->
                        <hr>
                        <p class="badge badge-pill badge-warning text-center mb-3" style="font-size:24px;">รางวัลประจำวันที่ 11 กันยายน 2563</p>
                        <img class="d-block w-100 mb-3" src="src/images/reward/reward2.jpg" alt="Second slide" width="100%" height="100%">
                        <hr>
                        <p class="badge badge-pill badge-warning text-center mb-3" style="font-size:24px;">รางวัลประจำวันที่ 14 กันยายน 2563</p>
                        <img class="d-block w-100 mb-3" src="src/images/reward/reward3.jpg" alt="Second slide" width="100%" height="100%">
                        <hr>
                        <p class="badge badge-pill badge-warning text-center mb-3" style="font-size:24px;">รางวัลประจำวันที่ 15 กันยายน 2563</p>
                        <img class="d-block w-100 mb-3" src="src/images/reward/reward4.jpg" alt="Second slide" width="100%" height="100%">
                        <hr>
                        <p class="badge badge-pill badge-warning text-center mb-3" style="font-size:24px;">รางวัลประจำวันที่ 16 กันยายน 2563</p>
                        <img class="d-block w-100 mb-3" src="src/images/reward/reward5.jpg" alt="Second slide" width="100%" height="100%">
                        <!-- <img id="rewardpic" class="d-block w-100" hidden src="src/images/reward/reward1.jpg" alt="First slide" width="100%" height="100%"> -->

                        <!-- <div id="carouselExampleControls" class="carousel slide" data-ride="carousel"> -->
                        <!-- <div id="carouselExampleControls" class="carousel slide" data-interval="false">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <h4 class="text-center">รางวัลประจำวันที่ 10 กันยายน 2563</h4>
                                    <img class="d-block w-100" src="src/images/reward/reward1.jpg" alt="First slide" width="100%" height="100%">
                                </div>
                                <div class="carousel-item">
                                    <h4 class="text-center">รางวัลประจำวันที่ 11 กันยายน 2563</h4>
                                    <img class="d-block w-100" src="src/images/reward/reward2.jpg" alt="Second slide">
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div> -->
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button onclick="onRewardClose()" type="button" class="btn" data-dismiss="modal" style="color:white; background-color:#4a774f">ปิด</button>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="loginmodal" tabindex="-1" role="dialog" aria-labelledby="logintitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="logintitle">กรุณา login เข้าสู่ระบบ</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="m-3 modal-body d-flex justify-content-center">
                    <div class="text-center">
                        <div>
                            <a onClick="popupLogin('medmail')" style="cursor:pointer; font-size:28px;"><img src="https://medhr.medicine.psu.ac.th/SingleSign/image/med.gif" width="150"> Medicine Mail</a>
                        </div>
                        <div class="mt-3 mb-3">
                            <h3 style="color:crimson">หรือ</h3>
                        </div>
                        <div>
                            <form class="p-3" style="background-color:#eee; border-radius:20px;">
                                <div class="form-group">
                                    <label for="perid" class="col-form-label">รหัสบุคลากร(PERID)</label>
                                    <input type="number" class="form-control" name="perid" id="perid" style="text-align: center;">
                                </div>
                                <div class="form-group">
                                    <label for="pin" class="col-form-label">PIN</label>
                                    <input type="password" class="form-control" name="pin" id="pin" style="text-align: center;">
                                </div>
                                <input class="btn btn-info" type="submit" value="เข้าสู่ระบบ" style="color:white; background-color:#4a774f">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="answersuccess" tabindex="-1" role="dialog" aria-labelledby="answersuccesstitle" aria-hidden="true" data-backdrop="static" data-keyboard="false" >
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="answersuccesstitle">คุณได้ตอบคำถามประจำวันที่</h5>
                    <h5 class="modal-title" id="answerdate" style="color:crimson;"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body d-flex justify-content-center">
                    <div class="card-body">
                        <h5 class="card-title" id="quizdatelabel" ></h5>
                        <!-- <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6> -->
                        <!-- <p class="card-text" id="answerdata">Some quick example text to build on the card title and make up the bulk of the card's content.</p> -->
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button onclick="onClose()" type="button" class="btn" data-dismiss="modal" style="color:white; background-color:#4a774f">ปิด</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="waitingquestion" tabindex="-1" role="dialog" aria-labelledby="waitingtitle" aria-hidden="true" data-backdrop="static" data-keyboard="false" >
        <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header d-flex justify-content-center">
                    <h5 class="modal-title" id="waitingtitle">กรุณารอคำถามประจำวัน...</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!-- <div class="modal-body d-flex justify-content-center">
                    <h5 class="card-title" id="quizdatelabel" >กรุณารอคำถามประจำวัน...</h5>
                </div> -->
                <div class="modal-footer d-flex justify-content-center">
                    <button onclick="onClose()" type="button" class="btn" data-dismiss="modal" style="color:white; background-color:#4a774f">ปิด</button>
                </div>
            </div>
        </div>
    </div>

</body>

<script>
    
    const perid ='<?php echo $_SESSION['perid']; ?>';
    const email ='<?php echo $_SESSION['email']; ?>';
    let token ='<?php echo $_SESSION['token']; ?>';
    let mobilecookie = '<?php echo $_GET['token'] ?>';
    let cookie = getCookie("token");
    let index = 0;
    let questions = [];
    let answerList = {
        method:"PUT",
        data:[]
    };

    $( "form" ).submit(function( event ) {
        let loginbody = $(this).serializeArray();
        let perid = loginbody[0].value;
        let pin = loginbody[1].value;
        let body = {
            perid: perid,
            pin: pin
        }
        login(body).then(promres => {
            if (promres.hasOwnProperty("access_token")) {
                let token = promres.access_token;
                refreshtoken(token).then(refreshpromres => {
                    // console.log(refreshpromres);
                    document.cookie = "token="+refreshpromres.token;
                    cookie = getCookie("token");
                    if (cookie != "") {
                        $('#loginmodal').modal('hide');
                        window.location.href;
                        $("#infomodal").modal("show");
                    }
                });
            }else{
                Swal.fire({
                    icon: 'warning',
                    title: 'รหัสบุคลากร หรือ pin ไม่ถูกต้อง',
                    text: 'กรุณาตรวจสอบ รหัสบุคลากร และ pin ของท่านให้ถูกต้องแล้วเข้าสู่ระบบใหม่อีกครั้ง',
                    confirmButtonText:"ตกลง",
                    showClass: {
                        popup: 'animate__animated animate__fadeInDown'
                    },
                    hideClass: {
                        popup: 'animate__animated animate__fadeOutUp'
                    },
                })
            }
        })
        event.preventDefault();
    });

    $(document).ready(function() {
        if (token != '' || mobilecookie != '' || cookie != '') {
            // window.history.back();
            // console.log(window.history.back());
            // Login Success
            // window.location.href = "http://172.29.70.129/medquiz/notfound.php";
            window.location.href;
            $("#infomodal").modal("show");
            // checkAnswerStatus(token);
            // $("#medpersonid").text("Perid: " + perid);
        } else{
            // Please Login
            // console.log("login failed");
            $('#loginmodal').modal('show');
        }
    });
    
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
            c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    function goToQuiz(){
        if (cookie != '') {
            token = cookie;
        }else if (mobilecookie != '') {
            token = mobilecookie;
        }
        $("#infomodal").modal("hide");
        checkAnswerStatus(token);
    }

    function goToReward(){
        $("#infomodal").modal("hide");
        $("#rewardmodal").modal("show");
    }

    function checkAnswerStatus(token){
        getQuizAnswer(token).then(promres => {
            if (promres.data.length == 0) {
                // console.log("ยังไม่ได้ตอบคำถาม");
                onStart();
            } else{
                // console.log("ตอบคำถามไปแล้ว");
                $("#answersuccess").modal("show");
                // console.log(promres.data);
                let newDate = new Date();
                let curMonth = newDate.getMonth() + 1;
                let curDate = newDate.getDate();
                if (curMonth <= 9) {
                    curMonth = "0" + curMonth;
                }

                if (curDate <= 9) {
                    curDate = "0" + curDate;
                }

                let fullDate = convertToDateTH(newDate.getFullYear() + "-"+ curMonth + "-" + curDate);
                $("#answerdate").text(fullDate);
                let answerlabel = "";
                promres.data.forEach((element, i) => {
                    answerlabel = answerlabel +" ข้อที่ "+ (i+1) + " ตอบ " + element.choiceSeq + "<br>";
                });
                $("#quizdatelabel").html("<b>คำตอบของคุณ คือ</b><br>"+ answerlabel);
                document.getElementById("nextbtn").style.visibility = "hidden";
            }
        })
    }

    function onStart(){
        getQuestions().then(questionpromdata => {
            let qData = questionpromdata.data;
            if (qData.length != 0) {
                $("#question").text(questionpromdata.data[index].question);
                //let quizid = questionpromdata.data[index].quiz_id;
                $("#waitingquestion").modal("hide");
                qData.forEach((questionelement,qIndex) => {
                    questions.push(
                        {
                            "quiz_id": questionelement.quiz_id,
                            "question": questionelement.question,
                            "choices": []
                        }
                    )
                    getChoices(questionelement.quiz_id).then((choicepromdata) => {
                        let choicelist = choicepromdata.data;
                        choicelist.forEach((element,i) => {
                            questions[qIndex]["choices"].push(choicelist[i].choiceLabel);
                        });

                        if (qIndex == 0) {
                            for (let choiceindex = 0; choiceindex < questions[0]["choices"].length; choiceindex++) {
                                const element = questions[0]["choices"][choiceindex];
                                let cIndex = choiceindex+1;
                                let label =  "<label id='answer"+cIndex+"' class='btn btn-lg btn-success btn-block'>"
                                                + "<input type='radio' id='radio"+cIndex+"' name='answer' value='"+cIndex+"' style='visibility:hidden;'>"
                                                + "<i class='fas fa-circle' style='font-size: 18px;'></i>"
                                                + "&nbsp;"
                                                + "&nbsp;"
                                                + "&nbsp;"
                                                + "<label id='choice"+cIndex+"'></label>"
                                                + "</label>";
                                $("#choices").append(label);
                                $("#choice"+cIndex).text(element);
                            }
                        }
                    })

                    // Default Question 1
                    if (qIndex == 0) {
                        $("#questionstep").append("<div id='section"+(qIndex+1)+"' class='col step-btn-enable mr-1'></div>");
                    }
                    else{
                        $("#questionstep").append("<div id='section"+(qIndex+1)+"' class='col step-btn-disable mr-1'></div>");
                    }

                    document.getElementById("nextbtn").style.visibility = "visible";

                });
            } else{
                $("#waitingquestion").modal("show");
            }
        });
    }

    function onNext(){
        if (index < questions.length) {
            //console.log(questions);
            //console.log(questions[index].quiz_id);
            onChangeStep(questions[index].quiz_id);
        } else{
            $("#nextbtn").text("ส่งคำตอบ");
        }

    }

    function onChangeStep(quizid){
        let answer = $('input[name=answer]:checked').val();
        // console.log("ตอบ: " + answer);
        // console.log("============================");
        if (typeof answer !== "undefined") {
            $("#answer"+answer).removeClass("active");
            $('input[name=answer]:checked').prop('checked', false);
            index = index + 1;
            answerList.data.push({
                "quiz_id": quizid,
                "choiceSeq": parseInt(answer),
            });
            if (index < questions.length) {
                if (index == questions.length -1) {
                    $("#nextbtn").text("ส่งคำตอบ");
                }
                //Disable Step ก่อนหน้า
                $("#section"+ (index)).removeClass("col step-btn-enable mr-1");
                $("#section"+ (index)).addClass("col step-btn-success mr-1");

                //Enable Step ถัดไป
                $("#section"+ (index+1)).removeClass("col step-btn-disable mr-1");
                $("#section"+ (index+1)).addClass("col step-btn-enable mr-1");

                // console.log("คำถามข้อที่ "+ (index+1));

                $("#question").text(questions[index].question);

                $("#choices").empty();
                questions[index].choices.forEach((element,i) => {
                    // $("#choice"+(i+1)).text(element);
                    let cIndex = i+1;
                    let label =  "<label id='answer"+cIndex+"' class='btn btn-lg btn-success btn-block'>"
                                            + "<input type='radio' id='radio"+cIndex+"' name='answer' value='"+cIndex+"' style='visibility:hidden;'>"
                                            + "<i class='fas fa-circle' style='font-size: 18px;'></i>"
                                            + "&nbsp;"
                                            + "&nbsp;"
                                            + "&nbsp;"
                                            + "<label id='choice"+cIndex+"'></label>"
                                            + "</label>";
                    $("#choices").append(label);
                    $("#choice"+cIndex).text(element);
                });


            } else{
                // console.log("หมดคำถาม");
                onFinish();
            }
        } else{
            // console.log("กรุณาเลือกคำตอบ");
            Swal.fire({
                icon: 'error',
                title: 'ยังไม่ได้เลือกคำตอบ',
                text: 'กรุณาเลือกคำตอบก่อนจะไปข้อถัดไป',
                showClass: {
                    popup: 'animate__animated animate__fadeInDown'
                },
                hideClass: {
                    popup: 'animate__animated animate__fadeOutUp'
                },
            })
        }
    }

    function onFinish(){
        console.log(answerList);
        $("#section"+ (index)).addClass("col step-btn-success mr-1");
        getQuizAnswer(token).then(promres => {
            if (promres.data.length == 0) {
                // console.log(answerList);
                sendAnswer(token, answerList).then(promres => {
                    if (promres.status) {
                        Swal.fire({
                            title: 'ส่งคำตอบแล้ว',
                            text: '',
                            imageUrl: 'src/images/mail.gif',
                            imageWidth: 300,
                            imageHeight: 200,
                            imageAlt: 'Custom image',
                            showClass: {
                                popup: 'animate__animated animate__fadeInDown'
                            },
                            hideClass: {
                                popup: 'animate__animated animate__fadeOutUp'
                            },
                        }).then(result => {
                            if (result.value) {
                                // console.log("Refresh แล้วตรวจสอบว่า User คนนี้ได้ทำการตอบคำถามแล้วหรือยัง");
                                clearData();
                                checkAnswerStatus(token);
                            }
                        });
                    } else{
                        Swal.fire({
                            title: 'ไม่สามารถส่งคำตอบของคุณได้',
                            text: 'เนื่องจากพบปัญหาบางประการไม่สามารถส่งคำตอบของคุณได้ กรุณาตอบคำถามแล้วส่งใหม่อีกครั้ง',
                            icon: 'error',
                            // imageUrl: 'src/images/mail.gif',
                            // imageWidth: 300,
                            // imageHeight: 200,
                            // imageAlt: 'Custom image',
                            showClass: {
                                popup: 'animate__animated animate__fadeInDown'
                            },
                            hideClass: {
                                popup: 'animate__animated animate__fadeOutUp'
                            },
                        });
                    }
                })
            } else{
                // console.log("ตอบคำถามไปแล้ว");
                $("#answersuccess").modal("show");
                // $("#answersuccesstitle").text("ไม่สามารถส่งคำตอบซ้ำได้");
                // document.getElementById("nextbtn").style.visibility = "hidden";
            }
        })
    }

    function clearData(){
        index = 0;
        questions = [];
        answerList = {
            method:"PUT",
            data:[]
        };
        $("#questionstep").empty();
        $("#question").empty();
        $("#choices").empty();
    }

    function convertToDateTH(date) {
        let dateNow = new Date(date.replace(" ", "T"));
        let dateTH = dateNow.getDate();
        let monthTH = dateNow.getMonth() + 1;
        let yearTH = dateNow.getFullYear() + 543;
        let dateTHStr;
        let monthTHStr;
        let fulldateTH;
        if (parseInt(dateTH) < 9) {
            dateTHStr = "0" + dateTH;
        } else {
            dateTHStr = dateTH;
        }
        switch (monthTH) {
            case 1:
                monthTHStr = "มกราคม";
                break;
            case 2:
                monthTHStr = "กุมภาพันธ์";
                break;
            case 3:
                monthTHStr = "มีนาคม";
                break;
            case 4:
                monthTHStr = "เมษายน";
                break;
            case 5:
                monthTHStr = "พฤษภาคม";
                break;
            case 6:
                monthTHStr = "มิถุนายน";
                break;
            case 7:
                monthTHStr = "กรกฎาคม";
                break;
            case 8:
                monthTHStr = "สิงหาคม";
                break;
            case 9:
                monthTHStr = "กันยายน";
                break;
            case 10:
                monthTHStr = "ตุลาคม";
                break;
            case 11:
                monthTHStr = "พฤศจิกายน";
                break;
            case 12:
                monthTHStr = "ธันวาคม";
                break;
            default:
                break;
        }
        fulldateTH = dateTHStr + " " + monthTHStr + " " + yearTH;
        return fulldateTH;
    }

    function onClose(){
        $("#answersuccess").modal("hide");
        $("#waitingquestion").modal("hide");
        $("#infomodal").modal("show");
    }

    function onShowReward(index){
        console.log(index);
    }

    function onRewardClose(){
        $("#rewardmodal").modal("hide");
        $("#infomodal").modal("show");
    }

</script>

</html>
